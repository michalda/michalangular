import { Component, OnInit } from '@angular/core';
import {User} from './user'

@Component({
  selector: 'jce-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css'],
  inputs:['user']
})
export class UserComponent implements OnInit {
  user;
  constructor() { }

  ngOnInit() {
  }

}